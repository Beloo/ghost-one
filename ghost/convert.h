#include <string>
#include <algorithm>
#include <windows.h>
#include <stdio.h>

#pragma once

using namespace std;

// Prototype for conversion functions
wchar_t * utf8_to_unicode(string utf8_string);
char * unicode_to_1251(wchar_t *unicode_string);
wchar_t * ansi_to_unicode(string ansi_string);
char * unicode_to_utf8(const wchar_t *unicode_string);