#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#pragma once

using namespace std;

class maps
{
public:
	int err;
	maps();
	~maps();
	void Wlist(string folder, vector<string> &List);
};

int SearchFilebyPart (string namepart, string path, string &FoundMapConfigs, string &LastMatchPath);
string CutPath(string path,int num,bool frombegin);

bool GetObjectsByPart(vector<string> &FoundObjects , string type, string path,string part,int *Matches );

#define  GetObjects(FoundObjects,type,path) GetObjectsByPart(FoundObjects,type,path, "", NULL)
