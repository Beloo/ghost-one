#include "text.h"

int CutWords( string Str, string separators, vector<string> &words)
{
	int i=0,beginW=0,endW,numW=0;
	words.clear();
	while (1)
	{
		endW = Str.find_first_of(separators,beginW);
		if (endW == string.npos)
		{
			numW++;
			words.push_back(Str.substr(beginW,Str.length()-beginW));
			break;
		}
		words.push_back( Str.substr(beginW,endW-beginW));
		numW++;
		beginW=endW+1;
	}
	return numW;
}

string RemMergedSpaces (string Str)
{
	unsigned int i=1;
	while (Str[0]==' ')
		Str.erase(0);
	while (Str[Str.size()-1]==' ')
		Str.erase(Str.size()-1);
	while (i<Str.size())
	{
		if (Str[i]==' ' && Str[i-1]==' ')
			Str.erase(i);
		else
			i++;
	}
	return Str;
}
