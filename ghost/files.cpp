#include <windows.h>
#include <tchar.h> 
#include <stdio.h>
#include <strsafe.h>

#include <iostream>
#include <fstream>
#include <algorithm>
#include "files.h"
#include "convert.h"
#include <vector>
#include <string>

using namespace std;
 
maps::maps()
{
 err=0;
}
maps::~maps()
{ err=0;}

bool SkipNames(string TekName)
{
	if (TekName == "common" || TekName == "blizzard" || TekName == "map" )
		return 1;
	return 0;
}

bool GetObjectsByPart(vector<string> &FoundObjects , string type, string path, string part, int *Matches )
{
	WIN32_FIND_DATAA ffd;
	HANDLE hFind ;
	DWORD dwError=0;
	string objname;
	FoundObjects.clear();
	path+="*.*";
	if ( part!="")
	{
		*Matches=0;
		transform( part.begin( ), part.end( ), part.begin( ), (int(*)(int))tolower );
	}
	hFind=FindFirstFileA(path.c_str(), &ffd );
	if (INVALID_HANDLE_VALUE == hFind) 
	   return 0;
	do
    {
		 objname = ffd.cFileName;
		 if( (objname!=".") && (objname!=".."))
		 {
			if ((ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) && (type=="DIR"))
			if ( part =="")
				FoundObjects.push_back(objname);
			else
			{
				string lowfoldername = objname;
				transform( lowfoldername.begin( ), lowfoldername.end( ), lowfoldername.begin( ), (int(*)(int))tolower );
				if (lowfoldername.find(part)!=string::npos)
				{
					*Matches=*Matches +1;
					FoundObjects.push_back(objname);
				}
			}
			if (!(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) && (type=="FILE"))
			if ( part =="")
				FoundObjects.push_back(objname);
			else
			{
				string lowfilename = objname;
				transform( lowfilename.begin( ), lowfilename.end( ), lowfilename.begin( ), (int(*)(int))tolower );
				if (lowfilename.find(part)!=string::npos)
				{
					*Matches=*Matches +1;
					FoundObjects.push_back(objname);
				}
			
			}
		 }
   }
   while (FindNextFileA(hFind, &ffd) != 0);
   dwError = GetLastError();
   if (dwError != ERROR_NO_MORE_FILES) 
	  return 0;
   FindClose(hFind);
   return 1;
}

void maps::Wlist(string folder, vector<string> &List)
{
   WIN32_FIND_DATAA ffd;
   List.clear();
   HANDLE hFind ;
   DWORD dwError=0;
   folder+="*.*";
   printf("%s",folder.c_str());
   string filename;
  
   // Prepare string for use with FindFile functions.  First, copy the
   // string to a buffer, then append '\*' to the directory name.

   // Find the first file in the directory.

   hFind = FindFirstFileA(folder.c_str(), &ffd);

   if (INVALID_HANDLE_VALUE == hFind) 
   {
	   err=1 ;return;
   } 
   
   // List all the files in the directory with some info about them.
  // int count=99;
   do
   {
		 filename = ffd.cFileName;
		 if( (filename!=".") && (filename!=".."))
		 {
			 char * point= strchr(ffd.cFileName,'.');
			 if (*(point+1)='c')
			 {
				 for (int i=0; i<4; i++)
					 *(point+i)=_T('');
			 }

			 if ( !SkipNames(ffd.cFileName))
			 {	
				List.push_back(filename);
				/*
				if (strlen(ffd.cFileName)+count+5>40)
				{
					fprintf(f1,"\n");
					count=strlen(ffd.cFileName)+2;
				}
				else
				{
					fprintf(f1,";");
					count=count+3+strlen(ffd.cFileName);
				}
				_ftprintf(f1,TEXT(" {%s}"), ffd.cFileName);*/
			 }
		 }
   }
   while (FindNextFileA(hFind, &ffd) != 0);
 //  fclose(f1);
   dwError = GetLastError();
   if (dwError != ERROR_NO_MORE_FILES) 
   {
	   err=1; return;
   } 
   FindClose(hFind);
   err=0;
}

string CutPath(string path,int num,bool frombegin) //mmmm �������� �� ������
{
	string ret;
	if (frombegin)
	{
		int pos = path.find_first_of('\\');
		num--;
		while (num>0)
		{
			pos = path.find_first_of(pos+1,'\\');
			num--;
		}
		ret = path.substr(0,pos);
	}
	else
	{
		int pos = path.find_last_of('\\');
		num--;
		while (num>0)
		{
			pos = path.find_last_of('\\',pos-1);
			num--;
		}
		ret = path.substr(pos,path.length()-1);
	}
	return ret;
}

int SearchFilebyPart (string namepart, string path, string &FoundMapConfigs, string &LastMatchPath)
{
   HANDLE hf;
   WIN32_FIND_DATAA ffd;
   int Matches=0;
   string filename;
   string Spath = path;
   Spath+="*.*";
   hf=FindFirstFileA(Spath.c_str(), &ffd );
   if(hf!=INVALID_HANDLE_VALUE)
   {
       do
       {
			filename = ffd.cFileName;
			if( (filename!=".") && (filename!=".."))
            {
                if(!(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
                {
					// ���� ������ ����
					int pos;
					string lowfilename = filename;
					transform( lowfilename.begin( ), lowfilename.end( ), lowfilename.begin( ), (int(*)(int))tolower );
					pos = filename.find_last_of('.');
					string ext = filename.substr(pos+1); 
					string name = filename.substr(0,pos);
					if ( ext=="cfg" && lowfilename.find(namepart)!=string::npos )
					{
						LastMatchPath = path + filename;
						Matches++;
						if( FoundMapConfigs.empty( ) )
							FoundMapConfigs = name;
						else
							FoundMapConfigs += ", " + name;
						if ( lowfilename == namepart)
							return -1;
					}
                }
                else
                {
					 int ret;
					 ret = SearchFilebyPart (namepart, path + (const char *)ffd.cFileName +"\\" , FoundMapConfigs, LastMatchPath);
					 if (ret == -1)
						 return -1;
					 else
						 Matches+=ret;
                }
                
             }
	   } while(FindNextFileA(hf,&ffd)!=0);
       FindClose(hf);  
   }
   return Matches;
}

